# Environment setup

## SSH key setup

To clone the project code, you'll need to use Git. To do this, you first need to set up an SSH key pair and import it into your Gitlab account. To do this, follow the steps below:

If you do not have an existing SSH key pair, generate a new one:
1. Open a terminal.
    ```bash
    ssh-keygen -t ed25519 -C "<comment>"
    ```
2. Press `Enter`. Output similar to the following is displayed:
    ```bash
    Generating public/private ed25519 key pair.
    Enter file in which to save the key (/home/user/.ssh/id_ed25519):
    ```
3. Accept the suggested filename and directory
4. Specify a passphrase (let empty):
    ```bash
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    ```

Next, add an SSH key to your GitLab account;
1. Copy the contents of your public key file
    ```bash
    cat ~/.ssh/id_ed25519.pub
    ```
2. Sign in to GitLab.
3. On the left sidebar, select your avatar.
4. Select Edit profile.
5. On the left sidebar, select SSH Keys.
6. Select Add new key.
7. In the Key box, paste the contents of your public key.
8. In the Title box, type a description, like `Gitlab workshop`.
9. Select Add key.

## Setting up working groups

To simulate a team, we invite you to form groups of 4 people, trying to mix people from different laboratories. One member of the group will be "Project Leader", creating a fork in your personal namespace:
1. On the project’s homepage (https://gitlab.lisn.upsaclay.fr/asard/scoring), in the upper-right corner, select `Fork`
2. For Project URL, select the personal namespace your fork should belong to.
3. Select Fork project.

GitLab creates your fork, and redirects you to the new fork’s page.

## Setting up colloraborating workflows

The `main` branch stores the official release history, and the `develop` branch serves as an integration branch for features. The Project leader creates a `develop` branch locally and push it to the server:

* Using SSH if utilizing Ethernet connection:
```bash
git clone git@serveur-gitlab.lisn.upsaclay.fr:[namespace]/scoring.git
cd scoring
```

* Using HTTPS if utilizing Wifi connection:
```bash
git clone https://gitlab.lisn.upsaclay.fr/[namespace]/scoring.git
cd scoring
```

```bash
git branch develop
git push -u origin develop
```

This branch will contain the complete history of the project, whereas `main` will contain an abridged version. Other developers should now clone the central repository and create a tracking branch for develop.

Now, add your collaborators to the project:
1. Select Manage > Members.
2. Select Invite members.
3. Add three peoples in your group to the project with following roles:
    * A developer: `Developer` role
    * A tester: `Developer` role
    * A code reviewer: `Maintainer` role

## Cloning the repository for collaborators:

1. Clone the repository

* Using SSH if utilizing Ethernet connection:
```bash
git clone git@serveur-gitlab.lisn.upsaclay.fr:[namespace]/scoring.git
cd scoring
```

* Using HTTPS if utilizing Wifi connection:
```bash
git clone https://gitlab.lisn.upsaclay.fr/[namespace]/scoring.git
cd scoring
```

The developer should create feature branchs from the latest `develop` branch.

## Resources and useful links
* SSH keys: https://docs.gitlab.com/ee/user/ssh.html
* Gitflow workflow: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

