# Scoring program

This TP is based on a real-life situation that will enable you to put into practice the concepts covered during the presentations. You are part of a team working on a project entitled "Scoring".

## Specifications

### Objective
The aim of this program is to compare the predictions from `predict.txt` against the correct answers in `solution.txt` and compute various metrics: accuracy, mean squared error(MSE).

### Format of files

* Both files should have the same number of lines.
* Each line contains a sequence of three numbers separated by spaces, where each number is either '0' or '1'.

Here is an example of the file:
```
1 0 0
0 0 1
1 0 0
0 0 1
0 0 1
0 1 0
1 0 0
0 0 1
1 0 0
1 0 0
0 0 1
0 0 1
0 0 1
1 0 0
0 0 1
```

### Metric: 
* **Accuracy**: It is the ratio of the number of correct predictions to the total number of predictions. For example, if out of 10 predictions, 8 are correct, the accuracy is 80% (or 0.8). A line in `predict.txt` is considered a "correct prediction" when its content matches exactly with the corresponding line in `solution.txt`.

To illustrate: Suppose a line in `solution.txt` is:
```
1 0 0
```
If the corresponding line in `predict.txt` is also 1 0 0, then it's a correct prediction. Conversely, if the corresponding line in `predict.txt` is 0 1 0 or 1 0 1 (or any sequence other than 1 0 0), then it's an incorrect prediction.

* **Mean squared error(MSE)**: It quantifies the difference between the predicted values and the actual values. For each line, the squared difference between the numbers in `solution.txt` and `predict.txt` is computed and then averaged over all lines. The formula for MSE is:
$$ MSE = \frac{1}{N} \sum_{i=1}^{N} (predicted_i - actual_i)² $$

Where $N$ is the total number of predictions.
Suppose, for a particular line:
* `solution.txt` has: 1 0 0
* `predict.txt` has: 1 1 0

So, for this line, the squared error is $$ (1 - 1)² + (1 - 0)² + (0 - 0)² = 1 $$ 
Note that the MSE between two files would be the average of the squared errors for all lines.

## Role-playing

**Objective**: Enhance the existing codebase that currently only implements accuracy as a metric. As the project leader, you are to provide a release version of the code with extended functionalities. As the project leader, your first task is to create a comprehensive list of additional functionalities and improvements needed for the project:
<ol type="a">
  <li>Implement additional metric: Mean Squared Error (MSE)</li>
  <li>Input validation: Ensure that the input files (`predict.txt` and `solution.txt`) are correctly formatted and handle possible errors gracefully.</li>
  <li>Logging: Implement logging to capture the process and any potential errors. This will help in debugging and maintenance. </li>
  <li>Testing: Introduce unit tests for each implemented metric and other functions. This ensures that any future changes do not break existing functionalities.</li>
  <li>Documentation: Create comprehensive documentation that covers: how to use the program, a brief explanation of each metric, version history, etc.</li>
  <li>Team allocation: Assign each task to a team member based on their expertise. Ensure each member is clear about their responsibilities and deadlines.</li>
  <li>Testing and quality assurance: Once all functionalities are implemented, run an extensive testing phase. This includes both manual testing and automated unit tests.</li>
  <li>Feedback loop: Allow a period for internal feedback. Let team members use the program and provide feedback, which might lead to some refinements.</li>
  <li>Prepare release version: After all the above steps, consolidate the code, and prepare a release version. Ensure it is packaged with all dependencies and is easy to install/use.</li>
  <li>Post-release maintenance: Even after the release, be prepared to fix any unforeseen bugs and possibly update the program based on user feedbacks or external contributions.</li>
</ol>

The following steps have been prepared and arranged sequentially for your execution:
* [Setup](0-Setup.md)
* [MSE](a-MSE.md)
* [Input validation](b-INPUT-VALIDATION.md)
* [Logging](c-LOGGING.md)
* [Release](i-RELEASE.md)

## Resources and useful links

* Git: 
  * [ASARD](resources/Git.pdf)
  * [ANF QUALITE LOGICIELLE 2023](https://gitlab.lisn.upsaclay.fr/asard/anf-qualite-logicielle/-/tree/main/ressources/presentations/slides?ref_type=heads): see files git.pdf and gitlab.pdf
